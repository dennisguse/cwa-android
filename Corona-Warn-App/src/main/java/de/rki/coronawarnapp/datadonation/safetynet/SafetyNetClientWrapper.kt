package de.rki.coronawarnapp.datadonation.safetynet

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import dagger.Reusable
import de.rki.coronawarnapp.datadonation.safetynet.SafetyNetException.Type
import de.rki.coronawarnapp.environment.EnvironmentSetup
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withTimeout
import okio.ByteString
import okio.ByteString.Companion.decodeBase64
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

@Reusable
class SafetyNetClientWrapper @Inject constructor(
    private val environmentSetup: EnvironmentSetup
) {

    suspend fun attest(nonce: ByteArray): Report {
        throw SafetyNetException(Type.ATTESTATION_REQUEST_FAILED, "Attestation not available")
    }

    private fun String.decodeBase64Json(): JsonObject {
        val rawJson = decodeBase64()!!.string(Charsets.UTF_8)
        return JsonParser.parseString(rawJson).asJsonObject
    }

    private suspend fun callClient(nonce: ByteArray): Unit =
        suspendCancellableCoroutine { cont -> }

    data class Report(
        val jwsResult: String,
        val header: JsonObject,
        val body: JsonObject,
        val signature: ByteArray
    ) {
        val nonce: ByteString? = body.get("nonce")?.asString?.decodeBase64()

        val apkPackageName: String? = body.get("apkPackageName")?.asString

        val basicIntegrity: Boolean = body.get("basicIntegrity")?.asBoolean == true
        val ctsProfileMatch = body.get("ctsProfileMatch")?.asBoolean == true

        val evaluationTypes = body.get("evaluationType")?.asString
            ?.split(",")
            ?.map { it.trim() }
            ?: emptyList()

        val error: String? = body.get("error")?.asString
        val advice: String? = body.get("advice")?.asString

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            other as Report
            if (jwsResult != other.jwsResult) return false
            return true
        }

        override fun hashCode(): Int = jwsResult.hashCode()
    }

    companion object {
        private const val TAG = "SafetyNetWrapper"
    }
}
