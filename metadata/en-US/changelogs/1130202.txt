1.13.2.2: Remove faulty GMS detection code

* New tab based navigation on the home screen for accessing the contact journal and the exposure history
* Exposure logging can now be turned back on again after a positive test. (upstream CWA)
* Reworked microG detection code, opening the correct microG instance should now always work
* Show a warning in the app in case the phone is not fully compatible with contact tracing
* Enable debug log recording functionality under App Information
