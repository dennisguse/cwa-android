* Fix a bug where you could get stuck on the launcher activity after upgrading microG
* Add Bulgarian Translation for the donation card
* Add German FAQ page (linked when the app is used in German)
* Changes from CWA 1.10.1:
  * Implement a Contact Diary
  * Fix days since last encounter being displayed incorrectly
