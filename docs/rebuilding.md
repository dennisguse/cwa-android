### How to rebuild Corona Contact Tracing Germany

The CCTG app can be built reproducibly. This means that you can re-run the build instructions and arrive at the very same binary that we publish[1].

The upstream CWA build was not reproducible before [cwa-app-android/#2800](https://github.com/corona-warn-app/cwa-app-android/pull/2800) was merged due to a non-deterministic android vector graphics → png generation. To workaround this, we [pre-generated](https://codeberg.org/corona-contact-tracing-germany/cwa-android/commit/bc605d6d7960fea2899c6cd1543fd789d5a6ff9a) all PNG graphics for CCTG and included them in the source tree, but this is now no longer necessary.

You can recreate the build by using the following docker container. The docker build system is mostly provided for convenience. Rebuilds should work with `fdroid build -l de.corona.tracing` on any properly configured build system[2].
```
cd docs
docker build --no-cache -t cctg-builder .
docker run -it --volume ${PWD}/repo:/repo cctg-builder
```

After that you should have the latest apk in ./repo

[1] Android apks are signed with an embedded signature. This means the final apk has to be reproduced with some trickery as a rebuilder is not in possession of the private key to re-generate the signature. Currently this works by recreating an unsigned build, transplanting signature of the apk to-rebuild and then verifying this signature on the newly assembled apk. If this passes, the apk is identical and we can publish it.[3]

[2] The docker build uses an experimental fdroidserver version which is able to automatically download the Android NDK dependency as well as generally having better defaults. In general you'll need:
  * some recent [fdroidserver](https://gitlab.com/fdroid/fdroidserver/) version
  * openjdk-8 (newer version might work, but weren't tested)
  * Android NDK version r21c
  * Android SDK (at least the [cmdline-tools](https://developer.android.com/studio/#downloads), the rest will be downloaded on-demand)
  * the [de.corona.tracing.yml](https://gitlab.com/fdroid/fdroiddata/-/blob/master/metadata/de.corona.tracing.yml) metadata file

[3] There's, unfortunately, more to this. The procedure described above works with Android signatures v1 (which are basically .jar signatures) but is [not yet implemented](https://gitlab.com/fdroid/fdroidserver/-/issues/797) for v2-v4 signatures. F-Droid instead continues to verify the v1 signature and if that succeeds publishes the original apk.
